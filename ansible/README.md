For applying the all process just run 

**git clone https://gitlab.com/alperencelik58/pulsar-challenge.git**

**cd ./pulsar-challenge/ansible**

**ansible-playbook site.yaml**

"site.yaml" has several parts which are

**1. Creating the kubernetes cluster (kubernetes.yaml)**

To create kubernetes cluster I wrote a simple  project called "Noobspray" which is inspired by kubespray. It creates the kubernetes cluster for the given inventory file. 

**2. Deploy nginx-ingress-controller (ingress-controller.yaml)**

Deploys the nginx-ingress-controller by helm.

**3. Deploy rook for storage (rook.yaml)**

Deploys "rook.io" by  helm

**4. Deploy Redis (redis.yaml)**

Deploys "Redis" by  helm 

**5. Deploy Pulsar (pulsar.yaml)**

Deploys Pulsar by a helm

**6. Build the project (builder.yaml)** 

To build the project, the code cloned to a dedicated virtual machine and then playbook start the build phase. At the end of build the packages are copied into kubernetes master for upcoming stages.

**7. Deploy Pulsar functions (pulsar-functions.yaml)**

To deploy the pulsar functions into pods the playbook uses kubectl commands to copy and run the needed files such as functions and sink file.

