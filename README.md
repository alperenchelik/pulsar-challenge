# deploy-pulsar-functions

This refers to the base for a pulsar functions project, additional code and configuration files are expected to deploy this project into a kubernetes cluster.

## Getting started

The purpose of this stage of the interview process is to see how you work through a real-world problem that a AxonNetworks DevOps engineer will encounter. We'll introduce the code content to be deployed in the Problem Setup section, so please read through the details below to receive guidance on the Pulsar documentation and the required structure of the app after the deployment.

## Problem Setup

Pulsar functions, is the Serverless Module, that Pulsar offers as an easy interface, to enable complex processing or transformation logic before the message produced by the producer is consumed by the consumer. Pulsar Functions, deployment process is rather challenging due to the fact that they are deployed via an operator pattern that is triggered by the Pulsar Admin CLI.  Deployment of Pulsar Functions via Gitlab CI/CD or an Automated Script is the core task in this assignment, also certain tasks have to be completed as prerequisite:
- Setup Helm.
- Set up a k8s cluster.
- Deploy Pulsar to the k8s cluster.
- Deploy Redis to the k8s cluster.
This repo consists of two pulsar functions transform the trigger value into a longer string and feed to the pulsar-io redis sink, to be persisted in redis.  Repo Content is as follows:
- Code of FirstFunc, pulsar function that takes the value of a string of your name.  Ex: if your name is "Arman", trigger the function by passing the "trigger value" of "Arman".
- Code of SecondFunction
- Config file of First Func
- Config file of Second Function
- Config file of Redis Sink.
- Packaged ".nar" file of built in Pulsar Redis Sink.
We expect these two functions and 1 sink to be up and running in a k8s cluster. Also, to make these Pulsar Functions in the desired state, a pulsar cluster should be running in the same k8s cluster; as well as a Redis instance that the aformentioned Pulsar Sink will feed to.
Please refer to the details Specifications.

## Specifications
#### Part 1: Start a Kubernetes Cluster
Please start a k8s cluster with a distribution of your choice.  Docs, guidance can be found at: https://pulsar.apache.org/docs/en/kubernetes-helm/.

#### Part 2: Deploy Pulsar to the k8s Cluster
Deploy Pulsar using Helm, using the following chart https://github.com/apache/pulsar-helm-chart/tree/master/charts/pulsar.

#### Part 3: Deploy Redis to the k8s Cluster
Deploy Redis using Helm, using the following chart https://github.com/bitnami/charts/tree/master/bitnami/redis.

#### Part 4: Deploy Pulsar Functions
1. Fork this repository.
2. Modify the Function Config Files, under resources directory.
3. Build the project using Makefile given in the repo.  Please feel free to modify.
4. Write an automated script or an Ansible Playbook to get the k8s cluster with Pulsar and Redis up and running locally.

Either:

5. Install Gitlab Runner to run your Gitlab jobs locally.
6. Create a .gitlab-ci.yml that builds, packages and deploys the Pulsar functions in the local k8s cluster.

Or:

7. Create an automated script to get the Pulsar Functions deployed to the k8s cluster.

## What to Submit
1. Automated script/playbook to get the k8s cluster up and running.
2. Automated script/playbook to get the Pulsar and Redis in the k8s cluster.
3. Automated script/playbook to get the Pulsar Functions in the k8s cluster.
4. Expand the current read me Solution section to explain how to run your code.
5. Send a merge request of your project fork.

## Bonus
Please setup a Prometheus & Grafana instance to monitor, Pulsar Functions deployed.

## Solution:

For applying the all process first install the requirements

yum install epel-release
yum install ansible 

And the needed module for "master"

yum install python3
pip3 install pyyaml

**git clone https://gitlab.com/alperencelik58/pulsar-challenge.git**

**cd ./pulsar-challenge/ansible**

**ansible-playbook site.yaml**

"site.yaml" has several parts which are

**1. Creating the kubernetes cluster (kubernetes.yaml)**

To create kubernetes cluster I wrote a simple  project called "Noobspray" which is inspired by kubespray. It creates the kubernetes cluster for the given inventory file. 

**2. Deploy nginx-ingress-controller (ingress-controller.yaml)**

Deploys the nginx-ingress-controller by helm.

**3. Deploy rook for storage (rook.yaml)**

Deploys "rook.io" by  helm

**4. Deploy Redis (redis.yaml)**

Deploys "Redis" by  helm 

**5. Deploy Pulsar (pulsar.yaml)**

Deploys Pulsar by a helm

**6. Build the project (builder.yaml)** 

To build the project, the code cloned to a dedicated virtual machine and then playbook start the build phase. At the end of build the packages are copied into kubernetes master for upcoming stages.

**7. Deploy Pulsar functions (pulsar-functions.yaml)**

To deploy the pulsar functions into pods the playbook uses kubectl commands to copy and run the needed files such as functions and sink file.



